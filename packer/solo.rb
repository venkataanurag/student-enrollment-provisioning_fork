file_cache_path "/var/chef/cache"
file_backup_path "/var/chef/backup"
cookbook_path "/home/ubuntu/student-enrollment-provisioning/chef/cookbooks"
role_path "/home/ubuntu/student-enrollment-provisioning/chef/roles"
log_level :info
verbose_logging false